package org.specvcs.common.resources.theme;

import org.eclipse.fx.ui.services.theme.Stylesheet;
import org.eclipse.fx.ui.services.theme.Theme;
import org.eclipse.fx.ui.theme.AbstractTheme;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

@Component(service=Theme.class)
public class DefaultTheme extends AbstractTheme {

	public DefaultTheme() {
	    super("default", "Default Theme", DefaultTheme.class.getClassLoader().getResource("css/default.css"));
	}
	
	@Reference(policy=ReferencePolicy.DYNAMIC,cardinality=ReferenceCardinality.MULTIPLE)
	public void bindStylesheet(Stylesheet stylesheet) {
		super.registerStylesheet(stylesheet);
	}
	
	public void unbindStylesheet(Stylesheet stylesheet) {
		super.unregisterStylesheet(stylesheet);
	}
}