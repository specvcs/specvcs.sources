package org.specvcs.common.application.mainwindow;

import javax.annotation.PostConstruct;

import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class DummyPart {
	@PostConstruct
	public void createNode(BorderPane pane) {
		pane.setCenter(new Label("SpecVCS"));
	}
}
